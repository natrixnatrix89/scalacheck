package quickcheck

import common._
import org.scalacheck._
import Arbitrary._
import Gen._
import Prop.{BooleanOperators, forAll}

import scala.annotation.tailrec

abstract class QuickCheckHeap extends Properties("Heap") with IntHeap {

  lazy val genHeap: Gen[H] = frequency(
    (1, const(empty)),
    (10, for {
      i <- arbitrary[A]
      h <- genHeap
    } yield insert(i, h))
  )

  lazy val randomlyConstructedHeap: Gen[H] = frequency(
    (1, const(empty)),
    (20, for {
      i <- arbitrary[A]
      h <- randomlyConstructedHeap
      choice <- arbitrary[Boolean]
    } yield {
      if (isEmpty(h)) insert(i, h)
      else if (choice) insert(i, h)
      else deleteMin(h)
    })
  )

  implicit lazy val arbHeap: Arbitrary[H] = Arbitrary(genHeap)

  property("gen1") = forAll { h: H =>
    val m = if (isEmpty(h)) 0 else findMin(h)
    findMin(insert(m, h)) == m
  }

  property("min1") = forAll { a: A =>
    val h = insert(a, empty)
    findMin(h) == a
  }

  property("the length of added elements") = forAll { a: List[A] =>
    val heap = a.foldLeft(empty)((h, a) => insert(a, h))
    // I should be able to get back to empty by deleting same amount of times
    isEmpty(a.foldLeft(heap)((h, _) => deleteMin(h)))
  }

  property("min number") = forAll { (a: A, b: A) =>
    findMin(insert(b, insert(a, empty))) == (a min b)
  }

  property("delete only") = forAll { a: A =>
    isEmpty(deleteMin(insert(a, empty)))
  }

  def toList(h: H): List[A] = {
    if (isEmpty(h)) Nil
    else findMin(h) :: toList(deleteMin(h))
  }

  def countMap(h: H): Map[A, Int] = {
    toList(h).foldLeft(Map[A, Int]().withDefaultValue(0))((map, a) => {
      val prev = map(a)
      map.updated(a, prev + 1)
    })
  }

  def isSortedL(h: H): Boolean = {
    val l = toList(h)
    l.sorted(ord) == l
  }

  def size(h: H): Int = {
    if (isEmpty(h)) 0
    else 1 + size(deleteMin(h))
  }

  def isSorted(h: H): Boolean = {
    @tailrec
    def aux(c: A, rest: H): Boolean = {
      if (isEmpty(rest)) true
      else if (ord.lt(findMin(rest), c)) false
      else aux(findMin(rest), deleteMin(rest))
    }
    if (isEmpty(h)) true
    else aux(findMin(h), h)
  }

  property("should be sorted") = forAll { h: H =>
    isSorted(h)
  }

  property("minimum of melded") = forAll { (h1: H, h2: H) =>
    (!isEmpty(h1) && !isEmpty(h2)) ==> (findMin(meld(h1, h2)) == (findMin(h1) min findMin(
      h2)))
  }

  property("meld size") = forAll { (h1: H, h2: H) =>
    size(meld(h1, h2)) == size(h1) + size(h2)
  }

  property("sorted melded heap") = forAll { (h1: H, h2: H) =>
    isSorted(meld(h1, h2))
  }

  property("sorted after delete") = forAll { h: H =>
    (!isEmpty(h)) ==> isSorted(deleteMin(h))
  }

  property("meld with empty") = forAll { h: H =>
    isSorted(meld(h, empty))
  }

  property("constructs in random order") =
    forAll(randomlyConstructedHeap)(isSorted)

  property("calling deleteMin reduces the size") = forAll { h: H =>
    (!isEmpty(h)) ==> (size(deleteMin(h)) == size(h) - 1)
  }

  property("calling insert increases the size") = forAll { (h: H, a: A) =>
    size(insert(a, h)) == size(h) + 1
  }

  property("deleteMin decreases instance count") = forAll { h: H =>
    (!isEmpty(h)) ==> {
      val a = findMin(h)
      countMap(deleteMin(h))(a) == countMap(h)(a) - 1
    }
  }

  property("insert increases instance count") = forAll { (h: H, a: A) =>
    (countMap(insert(a, h))(a) == countMap(h)(a) + 1)
  }

}
